
 
class Customer extends Thread {
 
     Barbershop barbershop;
     int id;
    
     boolean shouldStop = false;
     boolean threadStop = false;
     boolean haveJustSat = false;
   public Customer(Barbershop barbershop, int id) {
      this.barbershop = barbershop;
      this.id = id;
    
   }

 
    public Barbershop getBarbershop() {
        return this.barbershop;
    }
 
    public void setBarbershop(Barbershop barbershop) {
        this.barbershop = barbershop;
    }
 
    public int getID(){
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
   public void run() {
 
       barbershop.getHairCut(this);
       while(!threadStop){
        try{
               sleep(100);
        }catch(InterruptedException e){}
       if(shouldStop){
            threadStop = true;
            }
       }
   }
 
}
