import java.util.InputMismatchException;
import java.util.Scanner;


public class RunBarberShop {

	public static void main(String[] args) {
		
		System.out.println("Enter the number of chairs");
		
		int numberOfChairs;
		while(true){
			try{
				Scanner scanner = new Scanner(System.in);
				 numberOfChairs = (int)scanner.nextDouble();
				 if(numberOfChairs == 0){
					 System.out.println("the barbershop should have at least one chair\n Enter again");
					 continue;
				 }
					 
					 
				 break;
			}catch(InputMismatchException e){
				System.out.println("Wrong input");
				System.out.println("Enter number of chairs again");
			}
		}
		
		System.out.println("The multithreaded program started with "+numberOfChairs+" charis" );
		
		

		Barbershop barbershop = new Barbershop(numberOfChairs);

		Barber barber = new Barber(barbershop);
		barber.start();

		int i = 1;
		long rand_count = System.currentTimeMillis();

		while (barber.isRunning()) {
			Customer c = new Customer(barbershop, i);

			c.start();
			waitTime();
			i++;
			rand_count++;
		}

		waitTime();
		waitTime();
		waitTime();
		waitTime();
		System.exit(i);
	}

	public static void waitTime() {
		long start = System.currentTimeMillis();
		long end = 0;
		boolean stop = false;
		while (!stop) {
			end = System.currentTimeMillis();
			if ((end - start) > 1000)
				stop = true;
		}
	}
}
