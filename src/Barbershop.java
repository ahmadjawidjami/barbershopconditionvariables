import java.util.Random;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Barbershop {

	private boolean openForBusiness;
	private ReentrantLock shopLock;
	private Condition seated, cutting;
	private Condition waitingForCustomer;
	private int barbersAvailable, numberOfChairsAvailable;
	private Vector<Customer> customerList;
	private int waiting = 0;
	private Customer readyToCut;

	public Barbershop(int numberOfChairs) {

		customerList = new Vector<Customer>();

		openForBusiness = false;
		this.numberOfChairsAvailable = numberOfChairs;
		shopLock = new ReentrantLock(true);
		seated = shopLock.newCondition();
		cutting = shopLock.newCondition();
		barbersAvailable = 1;
		waitingForCustomer = shopLock.newCondition();

	}

	public void getHairCut(Customer currentCustomer) {

		if (waiting < numberOfChairsAvailable) {
			customerList.add(currentCustomer);
			waiting++;
			System.out.println("Customer " + currentCustomer.getID()
					+ ": arrived, sitting in the waiting room");
		} else if (waiting >= numberOfChairsAvailable) {
			System.out
					.println("Customer "
							+ currentCustomer.getID()
							+ " cannot cut his hair here; the barber shop is full, customer should go to anotehr barbershop");
			currentCustomer.shouldStop = true;
		}

		String me = currentCustomer.id + "";
		if (!openForBusiness) {
			System.out.println("shop not open for business!");
			return;
		}
		try {
			shopLock.lock();
			while (barbersAvailable == 0) {

				try {
					seated.await();
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}

			}

			barbersAvailable--;
			waitingForCustomer.signal();
			try {
				cutting.await();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}

			barbersAvailable++;
			seated.signalAll();
		} finally {
			shopLock.unlock();
		}
		currentCustomer.shouldStop = true;
		System.out
				.println("Customer " + readyToCut.id + " has left barbershop");
	}

	public void goToWork() {
		try {
			

			shopLock.lock();
			openForBusiness = true;
			while (openForBusiness) {
				try {
					if (waiting <= 0) {
						System.out.println("the Barber is sleeping now");
					}
					waitingForCustomer.await();
				} catch (InterruptedException ex) {
					if (!openForBusiness)
						break;
					else
						ex.printStackTrace();
				}
				readyToCut = customerList.get(0);
				System.out.println("The barber is cutting hair of customer "
						+ readyToCut.id);
				customerList.remove(0);
				waiting--;
				cutting.signal();
				try {
					Thread.sleep(randomInt(1, 2) * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} finally {
			shopLock.unlock();
		}
	}

	public static int randomInt(int s, int e) {
		int start = s;
		int end = e;
		Random random = new Random();
		long range = (long) end - (long) start + 1;

		long fraction = (long) (range * random.nextDouble());
		int randomNumber = (int) (fraction + start);

		return randomNumber;
	}

}
